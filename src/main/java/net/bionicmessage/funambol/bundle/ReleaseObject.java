/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.bionicmessage.funambol.bundle;

public class ReleaseObject implements Comparable {

    String product;
    String version;
    String changeLog;
    String location;
    long timestamp;
    String replaceFile;
    String outFile;
    String[] userConfigFiles;

    public ReleaseObject() {
    }

    /**
     * Returns the value of version.
     */
    public String getVersion() {
        return version;
    }

    /**
     * Sets the value of version.
     * @param version The value to assign version.
     */
    public void setVersion(String version) {
        this.version = version;
    }
    boolean isStable;

    /**
     * Returns the value of isStable.
     */
    public boolean getIsStable() {
        return isStable;
    }

    /**
     * Sets the value of isStable.
     * @param isStable The value to assign isStable.
     */
    public void setIsStable(boolean isStable) {
        this.isStable = isStable;
    }

    /**
     * Returns the value of changeLog.
     */
    public String getChangeLog() {
        return changeLog;
    }

    /**
     * Sets the value of changeLog.
     * @param changeLog The value to assign changeLog.
     */
    public void setChangeLog(String changeLog) {
        this.changeLog = changeLog;
    }

    /**
     * Returns the value of timestamp.
     */
    public long getTimestamp() {
        return timestamp;
    }

    /**
     * Sets the value of timestamp.
     * @param timestamp The value to assign timestamp.
     */
    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    /**
     * Returns the value of replaceFile.
     */
    public String getReplaceFile() {
        return replaceFile;
    }

    /**
     * Sets the value of replaceFile.
     * @param replaceFile The value to assign replaceFile.
     */
    public void setReplaceFile(String replaceFile) {
        this.replaceFile = replaceFile;
    }

    /**
     * Returns the value of outFile.
     */
    public String getOutFile() {
        return outFile;
    }

    /**
     * Sets the value of outFile.
     * @param outFile The value to assign outFile.
     */
    public void setOutFile(String outFile) {
        this.outFile = outFile;
    }

    /**
     * Returns the value of userConfigFiles.
     */
    public String[] getUserConfigFiles() {
        return userConfigFiles;
    }

    /**
     * Sets the value of userConfigFiles.
     * @param userConfigFiles The value to assign userConfigFiles.
     */
    public void setUserConfigFiles(String[] userConfigFiles) {
        this.userConfigFiles = userConfigFiles;
    }

    /**
     * Returns the value of product.
     */
    public String getProduct() {
        return product;
    }

    /**
     * Sets the value of product.
     * @param product The value to assign product.
     */
    public void setProduct(String product) {
        this.product = product;
    }

    /**
     * Returns the value of location.
     */
    public String getLocation() {
        return location;
    }

    /**
     * Sets the value of location.
     * @param location The value to assign location.
     */
    public void setLocation(String location) {
        this.location = location;
    }

    public String toString() {
        String str = String.format("Product: %s, Version %s, Time %s, Location %s, Replaces %s with %s \r\n%s",
                product,
                version,
                Long.toString(timestamp),
                location,
                replaceFile,
                outFile,
                changeLog);
        return str;
    }

    public int compareTo(Object o) {
        ReleaseObject r2 = (ReleaseObject) o;
        if (this.getTimestamp() > r2.getTimestamp()) {
            return -1;
        } else if (this.getTimestamp() == r2.getTimestamp()) {
            return 0;
        } 
        return 1;
    }
}