/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package net.bionicmessage.funambol.bundle;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author matt
 */
public class fnblGetMoveConfigs {

    public static void main(String[] args) {
        FileInputStream fis = null;
        String dsServerPath = args[0];
        File confDirRoot = new File(dsServerPath+File.separator+"config");
        try {
            File processFeedback = new File("fnblgetupdatedconfs.txt");
            fis = new FileInputStream(processFeedback);
            BufferedReader bf = new BufferedReader(new InputStreamReader(fis));
            String line = null;
            while((line = bf.readLine()) != null) {
                String[] filePaths = line.split(":");
                File temp = new File(filePaths[0]);
                File toReplace = new File(confDirRoot, filePaths[1]);
                toReplace.delete();
                temp.renameTo(toReplace);
            }
            bf.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
