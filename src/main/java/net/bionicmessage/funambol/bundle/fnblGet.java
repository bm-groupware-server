/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.bionicmessage.funambol.bundle;

import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.*;
import javax.xml.xpath.*;
import javax.xml.parsers.*;
import org.w3c.dom.*;

public class fnblGet {

    ArrayList<ReleaseObject> releasesForProduct = null;
    ArrayList<ReleaseObject> toUpdate = null;
    //URL repoURL = 
    static URL repoURL = null;
    static URL helloURL = null;
    static boolean useNonStable = true;
    File[] modules = null;
    File dsServerRoot = null;
    File tempConfDir = null;
    DocumentBuilderFactory dbf = null;
    DocumentBuilder db = null;
    Logger log = null;
    static BufferedReader console = null;
    File processFeedback = null;
    FileWriter pf = null;

    public fnblGet(String rootPath) throws Exception {
        dsServerRoot = new File(rootPath);
        modules = new File(dsServerRoot, "modules").listFiles();
        log = Logger.getLogger("fnblGet");
        /* ConsoleHandler ch = new ConsoleHandler();
        ch.setLevel(Level.ALL);
        log.addHandler(ch); */
        log.setLevel(Level.ALL);
        log.info(modules.length + " files found in modules dir");
        toUpdate = new ArrayList();
        tempConfDir = new File(System.getProperty("java.io.tmpdir") + File.separator + "fnblconfigs");
        tempConfDir.mkdirs();
        processFeedback = new File("fnblgetupdatedconfs.txt");
        pf = new FileWriter(processFeedback);
    }

    public void fillReleaseForProductArray() throws Exception {
        releasesForProduct = new ArrayList();
        dbf = DocumentBuilderFactory.newInstance();
        db = dbf.newDocumentBuilder();

        InputStream repoStream = repoURL.openStream();
        Document repoDoc = db.parse(repoStream);
        NodeList products = repoDoc.getElementsByTagName("product");
        for (int i = 0; i < products.getLength(); i++) {
            Element product = (Element) products.item(i);
            String name = product.getAttribute("name");
            NodeList releases = product.getElementsByTagName("release");
            for (int j = 0; j < releases.getLength(); j++) {
                Element release = (Element) releases.item(j);
                ReleaseObject rObj = new ReleaseObject();
                rObj.setProduct(name);
                rObj.setVersion(release.getAttribute("version"));

                boolean isStable = Boolean.valueOf(getStringValueFromElement(release, "isStable")).booleanValue();
                rObj.setIsStable(isStable);

                String changeLog = getStringValueFromElement(release, "changelog");
                rObj.setChangeLog(changeLog);

                String times = getStringValueFromElement(release, "timestamp");
                rObj.setTimestamp(Long.parseLong(times));

                String location = getStringValueFromElement(release, "location");
                rObj.setLocation(location);

                NodeList userConfigFiles = release.getElementsByTagName("userConf");
                String[] configs = new String[userConfigFiles.getLength()];
                for (int k = 0; k < userConfigFiles.getLength(); k++) {
                    Element uc = (Element) userConfigFiles.item(k);
                    configs[k] = uc.getTextContent();
                }
                rObj.setUserConfigFiles(configs);
                Element file = (Element) release.getElementsByTagName("file").item(0);
                rObj.setReplaceFile(file.getAttribute("replaces"));
                rObj.setOutFile(getStringValueFromElement(release, "file"));
                releasesForProduct.add(rObj);
            }
        }
        Collections.sort(releasesForProduct); /* Sort the array in desending order
     * so when we scan it the newest release
     * is the first we hit */

    }

    public void checkModules() {
        for (int i = 0; i < modules.length; i++) {
            File file = modules[i];
            if (file.isFile() && file.getName().contains(".s4j")) {
                log.finer("Module " + file.getName());
                ReleaseObject latest = getLatestReleaseForModule(file.getName());
                if (latest != null) {
                    log.fine("Latest release: " + latest.getVersion() + " ts:" + latest.getTimestamp());
                    long curTimestamp = file.lastModified() / 1000;
                    if (latest.getTimestamp() > curTimestamp) {
                        log.info(file.getName() + " has a newer version");
                        System.err.print("Do you wish to upgrade " + latest.getProduct() + " to " + latest.getVersion());
                        System.err.println(" ?");
                        String stabilityStatus = latest.getIsStable() ? "stable" : "non-stable";
                        System.err.println("This version is marked as " + stabilityStatus);
                        System.err.print("y/n?");
                        try {
                            if (console.readLine().equals("y")) {
                                toUpdate.add(latest);
                            }
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }
                    }
                }
            }
        }
    }

    /** Download all available updates and write the files */
    public void downloadUpdates() throws Exception {
        Iterator<ReleaseObject> u = toUpdate.iterator();
        while (u.hasNext()) {
            ReleaseObject update = u.next();
            File toUpdate = new File(dsServerRoot, "modules" + File.separator + update.getReplaceFile());
            URL toDownload = new URL(update.getLocation());
            URLConnection uc = toDownload.openConnection();
            int clength = uc.getContentLength();
            File tempFile = File.createTempFile("fnblupd", ".s4j");
            log.finer("Using temporary file: " + tempFile.getPath());
            FileOutputStream tempFileOutput = new FileOutputStream(tempFile);
            InputStream is = uc.getInputStream();
            int readSoFar = 0;
            while (readSoFar != clength) {
                byte[] temp = new byte[65535];
                int read = is.read(temp);
                readSoFar = readSoFar + read;
                tempFileOutput.write(temp, 0, read);
                float percentage = ((float) readSoFar / (float) clength) * 100;
                System.out.print("\rRead " + percentage + "% of " + clength);
            }
            System.out.println();
            is.close();
            tempFileOutput.close();
            boolean oldDeleted = toUpdate.delete();
            if (!oldDeleted) {
                throw new Exception("Old module " + toUpdate.getName() + " refused to delete");
            }
            tempFile.renameTo(toUpdate);
            tempFile.setLastModified(uc.getLastModified());
            moveConfigFilesForModule(update.getUserConfigFiles());
        }
        pf.flush();
        pf.close();
    }

    public void moveConfigFilesForModule(String[] configFiles) {
        for (int i = 0; i < configFiles.length; i++) {
            String string = configFiles[i];
            File configFile = new File(dsServerRoot, "config" + File.separator + string);
            if (configFile.exists()) {
                log.info("Moving " + configFile.getAbsolutePath() + " to:");
                File tempPath = new File(tempConfDir, string);
                tempPath.getParentFile().mkdirs();
                log.info(tempPath.getAbsolutePath());
                configFile.renameTo(tempPath);
                try {
                    pf.write(tempPath.getAbsolutePath() + ":" + string + "\r\n");
                } catch (IOException ex) {
                    log.log(Level.SEVERE, "Error writing feedback", ex);
                }
            }
        }
    }

    public ReleaseObject getLatestReleaseForModule(String file) {
        Iterator<ReleaseObject> it = releasesForProduct.iterator();
        while (it.hasNext()) {
            ReleaseObject current = it.next();
            if (current.getReplaceFile().equals(file)) {
                return current;
            }
        }
        return null;
    }

    public static void main(String[] args) {
        System.out.println("FnblGet updater for Funambol");
        if (args.length == 0) {
            System.out.println("Please specify the root path for ds-server");
        }
        if (args.length == 2) {
            useNonStable = Boolean.getBoolean(args[1]);
        }
        try {
            console = new BufferedReader(new InputStreamReader(System.in));
            helloURL = new URL("http://latest.bionicmessage.net/fnbl-get/latest.txt");
            InputStream is = helloURL.openStream();
            BufferedReader helloMsg = new BufferedReader(new InputStreamReader(is));
            System.out.println();
            System.out.println("Server says:");
            String lineIn = null;
            while ((lineIn = helloMsg.readLine()) != null) {
                System.out.println(lineIn);
            }
            helloMsg.close();
            System.out.println();
            System.out.println("Press enter to continue");
            console.readLine();
            fnblGet fg = new fnblGet(args[0]);
            repoURL = new URL("http://latest.bionicmessage.net/fnbl-get/repo.xml");
            fg.fillReleaseForProductArray();
            fg.checkModules();
            fg.downloadUpdates();
            System.out.println("Done");
        } catch (Exception ex) {
            Logger.getLogger(fnblGet.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    private String getStringValueFromElement(Element parent, String childTag) {
        NodeList child = parent.getElementsByTagName(childTag);
        Element tag = (Element) child.item(0);
        if (tag != null && tag.getFirstChild() != null && tag.getFirstChild().getNodeType() == Node.TEXT_NODE) {
            String value = tag.getFirstChild().getNodeValue();
            return value;
        }
        return "";
    }
}
