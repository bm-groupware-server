/*
* Post configuration utility for Funambol/Groupware sync server
* Copyright (C) 2008 Mathew McBride
*
* This program is free software; you can redistribute it and/or modify it under
* the terms of the GNU Affero General Public License version 3 as published by
* the Free Software Foundation with the addition of the following permission
* added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED
* WORK IN WHICH THE COPYRIGHT IS OWNED BY FUNAMBOL, FUNAMBOL DISCLAIMS THE
* WARRANTY OF NON INFRINGEMENT  OF THIRD PARTY RIGHTS.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
* FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
* details.
*
* You should have received a copy of the GNU Affero General Public License
* along with this program; if not, see http://www.gnu.org/licenses or write to
* the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
* MA 02110-1301 USA.
*
* You can contact Funambol, Inc. headquarters at 643 Bair Island Road, Suite
* 305, Redwood City, CA 94063, USA, or at email address info@funambol.com.
*
* The interactive user interfaces in modified source and object code versions
* of this program must display Appropriate Legal Notices, as required under
* Section 5 of the GNU Affero General Public License version 3.
*
* In accordance with Section 7(b) of the GNU Affero General Public License
* version 3, these Appropriate Legal Notices must retain the display of the
* "Powered by Funambol" logo. If the display of the logo is not reasonably
* feasible for technical reasons, the Appropriate Legal Notices must display
* the words "Powered by Funambol".
*/
package net.bionicmessage.funambol.bundle;
import com.funambol.admin.util.*;
import com.funambol.server.config.*;
import net.bionicmessage.funambol.httpauth.HTTPAuthenticationOfficer;
import net.bionicmessage.funambol.citadel.sync.CitadelSyncSource;
import net.bionicmessage.funambol.citadel.store.CtdlFnblConstants;

import com.funambol.framework.engine.source.ContentType;
import com.funambol.framework.engine.source.SyncSourceInfo;

import java.io.*;
import java.beans.XMLEncoder;
import java.util.Properties;
public class postConfig {
	public static final String HTTPAUTHBEAN = "net/bionicmessage/funambol/httpauth/HTTPAuthenticationOfficer.xml";
	/** Sets up a CitadelSyncSource and serializes it for submission
	 * @param server Citadel server (i.e uncensored.citadel.org)
	 * @param storeRootPath Root path for stores (i.e /opt/Funambol/stores)
	 */
	public static String setupSyncSource(String server, String storeRootPath) 
	throws Exception{
		String host = null;
		String port = null;
		if (server.contains(":")) {
			String[] srvParts = server.split(":");
			host = srvParts[0];
			port = srvParts[1];
		} else {
			host = server;
			port = "504";
		}
		String stPath = storeRootPath+File.pathSeparator+"mail";
		CitadelSyncSource css = new CitadelSyncSource();
        Properties syncSourceProps = new Properties();
		syncSourceProps.setProperty(CtdlFnblConstants.SERVER_HOST, server);
		syncSourceProps.setProperty(CtdlFnblConstants.SERVER_PORT, port);
		syncSourceProps.setProperty(CtdlFnblConstants.ROOM_MAIL, "Mail");
		syncSourceProps.setProperty(CtdlFnblConstants.STORE_LOC, stPath);
		SyncSourceInfo ssInfo = new SyncSourceInfo();
        ContentType[] ctypes = new ContentType[]{
            new ContentType("application/vnd.omads-email+xml", "1.2"),
            new ContentType("application/vnd.omads-folder+xml", "1.2")
        };
        ssInfo.setSupportedTypes(ctypes);
		css.setInfo(ssInfo);
		css.setSyncSourceProperties(syncSourceProps);
        css.setSourceURI("mail");
        css.setName("mail");
		return serializeObject(css);
	}
	/** Serialize an object 
	* @param obj Object to serialize
	* @return Object serialized to XML
	*/
	public static String serializeObject(Object obj) throws Exception {
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		XMLEncoder e = new XMLEncoder(bos);
		e.writeObject(obj);
		e.close();
		return bos.toString();
	}
	/** Sets up a HTTPAuthenticationOfficer bean for the specified URL 
	* and returns the serialized bean 
	* @param server The HTTP URL to authenticate against
	*/
	public static String setupAndSerializeHTTPAuth(String server) throws Exception {
		HTTPAuthenticationOfficer newOfficer = new HTTPAuthenticationOfficer();
		newOfficer.setAuthenticationPath(server);
		return serializeObject(newOfficer);
	}
	/** Main method */
	
	public static void main(String args[]) {	
		if (args.length < 1) {
			System.out.println("No arguments specified, exiting");
			System.out.println("Operations: ");
			System.out.println("usehttpauth [http://server:port/path] - Use HTTP Auth");
			System.out.println("usedefaultauth - Use the default UserProvisioningOfficer");
			System.out.println("queryofficer - Get bean for current officer");
			System.out.println("createcitadel [ctdlhost] [storeroot] - Creates a citadel sync source");
			return;
		}
		String operation = args[0];
		try {
			/* Load the server configuration */
			WSTools fnbl = new WSTools("http://localhost:8080/funambol/services/admin","admin","sa");
			ServerConfiguration config = (ServerConfiguration)fnbl.invoke("getServerConfiguration",null);
			EngineConfiguration eConfig = config.getEngineConfiguration();
			System.out.println("Current officer: "+eConfig.getOfficer());
			if (operation.equals("usehttpauth") && args.length > 1) {
				eConfig.setOfficer(HTTPAUTHBEAN);
				String[] setupArgs = new String[]{
					HTTPAUTHBEAN,
					setupAndSerializeHTTPAuth(args[1])
				};
				fnbl.invoke("setServerBean",setupArgs);
			}
			else if (operation.equals("usedefaultauth")) {
				eConfig.setOfficer(EngineConfiguration.DEFAULT_OFFICER);
			} else if (operation.equals("queryofficer")) {
				String officer = (String)fnbl.invoke("getServerBean",new String[]{eConfig.getOfficer()});
				System.out.println(officer);
			}
			// Save configuration
			fnbl.invoke("setServerConfiguration",new ServerConfiguration[]{config});
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
