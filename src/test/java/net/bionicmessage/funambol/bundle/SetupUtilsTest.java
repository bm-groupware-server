/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package net.bionicmessage.funambol.bundle;

import java.util.Properties;
import junit.framework.TestCase;

/**
 *
 * @author matt
 */
public class SetupUtilsTest extends TestCase {
    
    public SetupUtilsTest(String testName) {
        super(testName);
    }            

    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    public void testGetPathsForServer() throws Exception {
        Properties forCitadel = SetupUtils.getDefaultURLsForServer(1);
        System.out.println(forCitadel.toString());
    }
    public void testSetupCalendarSyncSource() throws Exception {
        String setup = SetupUtils.setupCalendarSyncSource("http://comalies.citadel.org",
                "/groupdav/Calendar", 
                "text/x-s4j-sife",
                "groupdav-s-cal",
                "/opt/Funambol/stores");
        System.out.println(setup);
    }
    public void testSetupContactSyncSource() throws Exception {
        String setup = SetupUtils.setupContactSyncSource("http://comalies.citadel.org",
                "/groupdav/Contacts", 
                "text/x-vcard",
                "groupdav-v-addr",
                "/opt/Funambol/stores");
        System.out.println(setup);
    }

}
