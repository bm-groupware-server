<%-- 
    Document   : wizard
    Created on : Dec 5, 2008, 5:31:02 PM
    Author     : matt
--%>
<% request.setAttribute("customscript", "wizard.js"); %>
<%@ include file="head.jsp" %>
<div id="content">
    <div id="input">
        <h1>Groupware setup wizard</h1>
        
        <p>This wizard will set up sync sources for certain GroupWare servers.</p>
        
        <div id="srvtype">
            <p>Server type: </p>
            <p><input type="radio" name="srvtype" value="citadel" id="citadel" />
                <label for="citadel">Citadel</label> <input type="radio" name="srvtype" value=
                                                            "egroupware" id="egroupware" /> <label for="egroupware">eGroupware 1.5+</label>
                <input type="radio" name="srvtype" value="opengroupware" id="opengroupware" />
            <label for="opengroupware">OpenGroupware</label></p>
        </div>
        <div>
            <p><b>GroupDAV Server root:</b></p>
            
            <p>This is generally the HTTP URL you would point your browser to access your servers web
            interface, with a subdirectory for GroupDAV. Examples are:</p>
            
            <ul>
                <li>http://your.citadel.server:2000/groupdav/</li>
                
                <li>http://your.egroupware.server/egroupware/groupdav.php/</li>
                
                <li>http://your.opengroupware.server/zidestore/</li>
            </ul>
            
            <p><input type="text" id="srvroot" size="35" /></p>
        </div>
        <div id="ctdl">
            <p><b>Citadel Server</b></p>
            <p>To use the Citadel email connector, the server needs to know where your
            actual Citadel server is. Typically this would be on the same server as above.</p>
            <p>Add &quot;:port&quot; if your Citadel server runs on a different port than 504</p>
            <p><input type="text" id="ctdlsrv" size="35" /></p>
        </div>
        
        <div id="authentication">
            <p><b>Authentication:</b></p>
            
            <p>Funambol can either authenticate against an internal database (which automatically
                creates accounts as users sync), or using our provided HTTP GroupWare authenticator.
                Chose Funambol authentication if non-groupware users will use this sync server as
            well</p>
            
            <p><input type="radio" name="auth" value="internal" id="internal" /> <label for=
                                                                                            "internal">Internal Funambol database</label> <input type="radio" name="auth" value=
                                                                 "http" id="http" checked="checked" /> <label for="http">GroupWare</label></p>
        </div>
        
        <div id="sub">
            <p><button id="submit">Apply Configuration</button></p>
        </div>
    </div>
    <div id="output">
        <p><img src="ajax-loader.gif" alt="loading animation"/></p>
        <p class="big">Please wait. The server is configuring itself.</p>
    </div>
    <div id="template">
        <table id="eventresult">
            <thead>
                <tr>
                <td>Event</td>
                <td>Result</td>
            </tr>
        </thead>
        </table>
    </div>
</div>
<%@ include file="footer.jsp" %>