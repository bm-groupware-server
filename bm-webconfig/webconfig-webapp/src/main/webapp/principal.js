// MIME types for notification view:
var mimeTypes = {"vCalendar 1.0": "text/x-vcalendar",
    "vCard":"text/x-vcard", 
    "iCalendar 2.0":"text/icalendar",
    "SIF XML Event":"text/x-s4j-sife", 
    "SIF XML Task":"text/x-s4j-sift", 
    "SIF XML Contact":"text/x-s4j-sifc",
    "Email Folder":"application/vnd.omads-folder+xml",
    "Email Message":"application/vnd.omads-email+xml",
    "Plain text":"text/plain"};

$(document).ready(initPrincipalApp);
var originalSearchResults = null;
var curSearchResults = null;
var currentPrincipalId = 0;
var currentPrincipalUser = null;
var currentPrincipalDevice = null;
var doesNotifyContainerExist = false;
var selectCTypeListChildren = null;
function initPrincipalApp() {
    $.getJSON("principals",principalSearchResults);
    $("#search").bind("keyup", filterSearchResults);
    // Set up the notify tester
    doesNotifyContainerExist = (document.getElementById("notify") != null);
    if (doesNotifyContainerExist) {
        $("#contentTypeSelect").jHelperTip(
        {trigger: "click",
        dC: "#contenttypepopup",
        autoClose: true}
        );
        $(".selectctype li").bind("click",contentTypeSelected);
        var selectctype = $(".selectctype li").get(0);
        selectCTypeListChildren = selectctype.getElementsByTagName("li");
        document.getElementById("notificationTest").addEventListener("click",testNotification,false);
     }
}
function filterSearchResults(event) {
    $(".nomatch").show();
    $(".nomatch").attr("class","");
    searchText = document.getElementById("search").value;
    $.each(originalSearchResults, filterListItem);
    $(".nomatch").hide();
}
function filterListItem(i, item) {
    var record = originalSearchResults[i];
    var listId = "plist_"+item[0];
    var user = record[1];
    if (user.search(searchText) == -1) {
        document.getElementById(listId).setAttribute("class","nomatch");
    }
}
function principalSearchResults(data) {
    originalSearchResults = data;
    curSearchResults = data;
    $("#plist").empty();
    $("#results").empty();
    $.each(data, principalListItem);
}
function principalListItem(i, item) {
    var listId = "plist_"+i+"_"+item[0];;
    var jListId = "#"+listId;
    var dataId = "data_"+item[0];
    var jDataId = "#"+dataId;
    var comboId = item[1]+"|"+item[2].substring(0,8)+"..";
    $("#plist").append("<div id=\""+listId+"\"><span>"+comboId+"</span></div>");
    var dataString = "<table id=\""+dataId+"\"><thead><tr><td>Principal ID</td><td>User</td><td>Device</td></tr></thead>";
    dataString += "<tr><td>"+item[0]+"</td><td>"+item[1]+"</td><td>"+item[2]+"</td></tr></table>";
    $("#results").append(dataString);
    $(jListId).jHelperTip({
        trigger: "hover",
	dC: jDataId,
	autoClose: true});
    $(jListId).bind("click", loadSyncsForPrincipal);
}
function loadSyncsForPrincipal(event) {
    $("#syncbody").empty();
    document.getElementById("notify").style.display = "block";
    var targetID = event.target.parentNode.id;
    targetID = targetID.replace("plist_","");
    ids = targetID.split("_");
    rowId = parseInt(ids[0]);
    principalID = parseInt(ids[1]);
    currentPrincipalUser = curSearchResults[rowId][1];
    currentPrincipalDevice = curSearchResults[rowId][2];
    $("tr.prow").remove();
    $.getJSON("principalSyncs", {id: principalID}, function(data){
        $.each(data, principalSyncRow);
        currentPrincipalId = principalID;
    });
}
function principalSyncRow(i, item) {
    var buttonString = "<button id=\"reset_"+item[0]+"\">Reset</button>";
    var jResetId = "#reset_"+item[0];
    var dataString = "<tr class=\"prow\"><td>"+item[0]+"</td><td>"+item[1]+"</td><td>"+item[2]+"</td>";
    dataString += "<td>"+item[3]+"</td><td>"+item[4]+"</td><td>"+buttonString+"</td></tr>";
    $("#syncbody").append(dataString);
    $(jResetId).bind("click", resetSync);
}
function resetSync(event) {
    var targetSyncId = event.target.id;
    targetSyncId = targetSyncId.replace("reset_","");
    var postData = {syncsource: targetSyncId,
        principalId: currentPrincipalId};
    $.ajax(
    {type: "POST",
        data: postData,
        url: "principalSyncs",
        error: function(xhttprequest, status, error) {
            alert("Error: "+xhttprequest.responseText)
        },
        success: function(data, textStatus) {
            var jparentRow = event.target.parentNode.parentNode;
            $(jparentRow).hide("slow");
            $(jparentRow).remove();
        }

    });
}
/* Functions for notification tester */
function contentTypeSelected(event) {
    var target = event.target;
    var typeName = target.textContent;
    var typeValue = mimeTypes[typeName];
    document.getElementById("contentTypeSelect").value = typeValue;
}
function testNotification(event) {
    // Put the pretty animation image in
    $("#notificationFeedback").empty();
    $("#notificationprogress p").appendTo("#notificationFeedback");
    var syncSourceSelector = document.getElementById("syncSourceSelect");
    var syncSource = syncSourceSelector.options[syncSourceSelector.selectedIndex].textContent;
    var contentType = document.getElementById("contentTypeSelect").value;
    var postData = {user: currentPrincipalUser,
        device: currentPrincipalDevice,
        syncsource: syncSource,
        contenttype: contentType};
    $.ajax(
    {type: "POST",
        data: postData,
        url: "notification",
        error: function(xhttprequest, status, error) {
            notificationFeedback(xhttprequest.responseText);
        },
        success: function(data, textStatus) {
            notificationFeedback(data);
        }
    });
}
function notificationFeedback(textdata) {
    var nf = document.getElementById("notificationFeedback");
    $(nf).empty();
    var preElement = document.createElement("pre");
    nf.appendChild(preElement);
    preElement.textContent = textdata;
}