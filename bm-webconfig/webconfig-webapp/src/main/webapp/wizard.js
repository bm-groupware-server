$(document).ready(createWizardApp);

// instance vars
var serverType = null;
var authType = "http";
function createWizardApp(event) {
$("#srvtype p input").bind("click", serverTypeChange);
$("#authentication p input").bind("click", authTypeChange);
$("#submit").bind("click", submitWizard);
}
function serverTypeChange(event) {
    var targetElement = event.target;
    serverType = targetElement.getAttribute("value");
    if (serverType == "citadel") {
        $("#ctdl").show();
    } else {
        $("#ctdl").hide();
    }
}
function authTypeChange(event) {
    var targetElement = event.target;
    authType = targetElement.getAttribute("value");
}
function submitWizard(event) {
    $("#input").hide("slow");
    var server = $("#srvroot").val();
    var ctdlsrv = $("#ctdlsrv").val();
    var postData = {
        type: serverType,
        auth: authType,
        server: server,
        ctdlsrv: ctdlsrv
    };
    $.ajax({
        type: "POST",
        url: "applyconfigs",
        data: postData,
        success: successfulSubmit,
        error: postFailure
    });
    $("#output").show("slow");
}
function successfulSubmit(data, textStatus) {
    $("#output").empty();
    $("#output").append("<p class=\"big\">Configuration successful</p>");
    var eventData = eval(data);
    // Move the template into the output
    var template = document.getElementById("template");
    var eventtemplate = document.getElementById("eventresult");
    template.removeChild(eventtemplate);
    document.getElementById("output").appendChild(eventtemplate);
    $.each(eventData, eventItem);
}
function eventItem(i, item) {
    var eventName = item[0];
    var eventResult = item[1];
    var failureReason;
    var resultTdClass = "";
    var cellId = "cellr_"+i;
    var jCellId = "#"+cellId;
    var errorContainer = null;
    if (item.length == 3) {
        resultTdClass = "failure";
    } else if (eventResult == "Success") {
        resultTdClass = "success";
    }
    // Build the row the 'DOM way', because I have had enough of string concatenations today
    var trNode = document.createElement("tr");
    var eventNameTd = document.createElement("td");
    eventNameTd.textContent = eventName;
    var eventResultTd = document.createElement("td");
    eventResultTd.setAttribute("class", resultTdClass);
    if (item.length == 2) {
        eventResultTd.textContent = eventResult;
    } else {
        var resultText = document.createTextNode(eventResult + " ");
        var hideSmall = document.createElement("small");
        hideSmall.textContent = "(toggle error details)";
        hideSmall.addEventListener("click",toggleEventErrorDisplay,false);
        eventResultTd.appendChild(resultText);
        eventResultTd.appendChild(hideSmall);
        var errorDetailsContainer = document.createElement("div");
        errorDetailsContainer.setAttribute("class", "errorbox");
        errorDetailsContainer.style.display = 'none';
        var errorDetailsPre = document.createElement("pre");
        errorDetailsContainer.appendChild(errorDetailsPre);
        errorDetailsPre.textContent = item[2];
        eventResultTd.appendChild(errorDetailsContainer);
    }
    trNode.appendChild(eventNameTd);
    trNode.appendChild(eventResultTd);
    var eventResultTable = document.getElementById("eventresult");
    eventResultTable.appendChild(trNode);
}
function toggleEventErrorDisplay(event) {
    var targetElement = event.target;
    // the above will be a 'small' element, the div to toggle is next to it
    var targetErrorDisplay = targetElement.nextSibling;
    var displayMode = targetErrorDisplay.style.display;
    if (displayMode == "none") {
        targetErrorDisplay.style.display = 'block';
    } else {
        targetErrorDisplay.style.display = 'none';
    }
}
function postFailure(xhttprequest, status, error) {
    $("#output").empty();
    $("#output").append("<p class=\"big\">An error was encountered during configuration</p>")
    $("#output").append("<pre>" + xhttprequest.responseText + "</pre>");
}