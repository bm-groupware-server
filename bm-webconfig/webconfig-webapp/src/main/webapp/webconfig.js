$(document).ready(createHandlers);
var currentSelectedPrincipal = null;
var currentSelectedSync = null;
function createHandlers() {
    $("#principals table tr").click(principalsHandler);
}

function principalsHandler(event) {
    var targetCell = event.target;
    var parentId = targetCell.parentNode.id;
    if (currentSelectedPrincipal) {
        currentSelectedPrincipal.removeClass("selected");
    }
    currentSelectedPrincipal = $("#"+parentId);
    currentSelectedPrincipal.addClass("selected");
    $("#principalsdata").load("principal_datashow.jsp", 
    {id: parentId}, function(responseText,status,xhttprequest) {
        $("#principalsdata table tr").click(principalSyncHandler); 
        $("#principalsdata tr td button").click(resetPrincipalSyncSource);
    });
    var currentSelectedSync = null;
}

function principalSyncHandler(event) {
    var targetCell = event.target;
    var parentId = targetCell.parentNode.id;
    if (currentSelectedSync) {
        currentSelectedSync.removeClass("selected");
    }
    currentSelectedSync = $("#"+parentId);
    currentSelectedSync.addClass("selected");
}
function resetPrincipalSyncSource(event) {
    var parentId = event.target.parentNode.parentNode.id;
    currentSelectedSync = $("#"+parentId);
    var request = $.ajax({
        type: "POST",
        url:"principalOps", 
        data: {operation: "reset", id:parentId},
        success: resetPrincipalSyncSourceCallback,
        error: function (xhttprequest, textStatus, errorThrown) {
            alert(xhttprequest.responseText);
        }
    });
    
}
function resetPrincipalSyncSourceCallback(data, textStatus) {
    currentSelectedSync.hide("slow");
}