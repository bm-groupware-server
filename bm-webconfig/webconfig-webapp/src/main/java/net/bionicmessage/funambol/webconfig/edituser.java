/*
 *   WebConfig - a web administration interface for the Funambol DS Server.
 *   Copyright (C) 2008 Mathew McBride
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.bionicmessage.funambol.webconfig;

import com.funambol.framework.server.Sync4jUser;
import com.funambol.server.admin.UserManager;
import com.funambol.server.config.Configuration;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import net.bionicmessage.funambol.configuration.UserUtils;

/**
 *
 * @author matt
 */
public class edituser extends HttpServlet {

    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/plain;charset=utf-8");
        PrintWriter out = response.getWriter();
        HttpSession session = request.getSession(true);
        String userToEdit = (String) request.getParameter("user");
        String curUser = (String) session.getAttribute("user");
        // Attempt to trap lame privledge esculation here as well
        boolean isAdministrator = (session.getAttribute("isadministrator") != null);
        boolean isAllowedToEdit = isAdministrator;
        if (!isAllowedToEdit &&
                curUser != null &&
                userToEdit != null &&
                curUser.equals(userToEdit)) {
            isAllowedToEdit = true;
        } else if (!isAllowedToEdit) {
            response.setStatus(500);
            out.println("Security error: Are you logged in with the right credentials?");
            out.close();
            return;
        }
        // With that out of the way.. Get the Sync4jUser we are trying to edit
        Sync4jUser editedUser = null;
        try {
            editedUser = UserUtils.getUserByUsername(userToEdit);
        } catch (Exception e) {
            response.setStatus(500);
            out.println("Error while retrieving user: "+e.getMessage());
            out.close();
            return;
        }
        if (editedUser == null) {
            response.setStatus(500);
            out.println("User not found. Trg for them again and resubmitting this form");
            out.close();
            return;
        }
        String fname = request.getParameter("fname");
        String lname = request.getParameter("lname");
        String email = request.getParameter("email");
        String pass1 = request.getParameter("pass1");
        editedUser.setFirstname(fname);
        editedUser.setLastname(lname);
        editedUser.setEmail(email);
        if (pass1.length() > 1) {
            editedUser.setPassword(pass1);
        }
        try {
            UserUtils.setUser(editedUser);
        } catch (Exception ex) {
            response.setStatus(500);
            out.println("Error saving user: "+ex.getMessage());
            out.close();
            return;
        }

        out.println("Success");
        out.close();
    // response.sendRedirect("useredit.jsp?user=" + userToEdit + "&edited=true");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     */
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
