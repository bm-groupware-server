/*
 *   WebConfig - a web administration interface for the Funambol DS Server.
 *   Copyright (C) 2008 Mathew McBride
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.bionicmessage.funambol.webconfig;

import com.funambol.framework.tools.beans.BeanException;
import com.funambol.server.config.Configuration;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import net.bionicmessage.funambol.clickatell.ClickatellSender;

/**
 *
 * @author matt
 */
public class smsconfig extends HttpServlet {

    private static final String CONFIGURED_BEAN = "smsnotify/bmclickatell/bmclickatell.xml";
    private static final String EXAMPLE_BEAN = "smsnotify/bmclickatell/bmclickatell.example.xml";

    protected ClickatellSender getBean(Configuration conf) throws BeanException {
        ClickatellSender sender = null;
        try {
            sender = (ClickatellSender) conf.getBeanInstanceByName(CONFIGURED_BEAN, false);
            return sender;
        } catch (Exception e) {
            // ignore
        }
        return (ClickatellSender) conf.getBeanInstanceByName(EXAMPLE_BEAN, false);
    }

    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        PrintWriter out = response.getWriter();
        try {
            if (session.getAttribute("isadministrator") == null) {
                throw new Exception("Not logged in as administrator");
            }
            Configuration c = Configuration.getConfiguration();
            ClickatellSender sender = getBean(c);
            Map values = new HashMap();
            values.put("httpaddr", sender.getClickatellURL());
            values.put("smsuser", sender.getClickatellUser());
            values.put("smsid", sender.getClickatellID());
            Gson gson = new Gson();
            out.println(gson.toJson(values, new TypeToken<Map<String, String>>() {
            }.getType()));
            response.setContentType("application/json;charset=utf-8");
        } catch (Exception e) {
            response.setContentType("text/plain;charset=utf-8");
            response.setStatus(500);
            e.printStackTrace(out);
        } finally {
            out.close();
        }
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        PrintWriter out = response.getWriter();
        boolean isAdministrator = (session.getAttribute("isadministrator") != null);
        try {
            Configuration c = Configuration.getConfiguration();
            ClickatellSender sender = getBean(c);
            String httpaddr = request.getParameter("httpaddr");
            String smsuser = request.getParameter("smsuser");
            String smspass = request.getParameter("smspass");
            String smsid = request.getParameter("smsid");
            sender.setClickatellURL(httpaddr);
            sender.setClickatellUser(smsuser);
            if (smspass.length() > 0) {
                sender.setClickatellPass(smspass);
            }
            sender.setClickatellID(smsid);
            c.setBeanInstance(CONFIGURED_BEAN, sender);
            out.println("Success");
            response.setContentType("text/plain;charset=utf-8");
        } catch (Exception e) {
          e.printStackTrace(out);
          response.setContentType("text/plain;charset=utf-8");
        } finally {
            out.close();
        }
    }

    /** 
     * Returns a short description of the servlet.
     */
    public String getServletInfo() {
        return "SMS sender configuration";
    }
}
