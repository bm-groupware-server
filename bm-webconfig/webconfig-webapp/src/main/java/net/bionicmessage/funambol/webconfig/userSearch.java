/*
 *   WebConfig - a web administration interface for the Funambol DS Server.
 *   Copyright (C) 2008 Mathew McBride
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.bionicmessage.funambol.webconfig;

import com.funambol.framework.filter.AllClause;
import com.funambol.framework.filter.Clause;
import com.funambol.framework.filter.WhereClause;
import com.funambol.framework.server.Sync4jUser;
import com.funambol.server.admin.UserManager;
import com.funambol.server.config.Configuration;
import com.google.gson.Gson;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import net.bionicmessage.funambol.configuration.UserUtils;

/**
 *
 * @author matt
 */
public class userSearch extends HttpServlet {

    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     */
    @SuppressWarnings("empty-statement")
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json;charset=UTF-8");
        String[][] respData = null;
        PrintWriter out = response.getWriter();
        HttpSession session = request.getSession();
        try {
            String searchName = request.getParameter("name");
            String curUser = (String) session.getAttribute("user");
            boolean isAdministrator = (session.getAttribute("isadministrator") != null);
            try {
                Sync4jUser[] sjus = null;
                if (isAdministrator) {
                    sjus = UserUtils.findUsersBeginningWith(searchName);
                } else {
                    sjus = new Sync4jUser[1];
                    sjus[0] = UserUtils.getUserByUsername(curUser);
                }
                respData = new String[sjus.length][];
                for (int i = 0; i < sjus.length; i++) {
                    Sync4jUser sync4jUser = sjus[i];
                    boolean isCurUser = (sync4jUser.getUsername().equals(curUser));
                    String cUser = null;
                    if (isCurUser) 
                        cUser = "true"; // ugly hack, but keeps type all the same
                    String[] userInfo = {sync4jUser.getUsername(),
                        sync4jUser.getLastname(),
                        sync4jUser.getFirstname(),
                        sync4jUser.getEmail(),
                        cUser
                    };
                    respData[i] = userInfo;
                }
            } catch (Exception ex) {
                response.sendError(500, ex.getMessage());
            }
            Gson gson = new Gson();
            out.println(gson.toJson(respData));
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     */
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
