/*
 *   WebConfig - a web administration interface for the Funambol DS Server.
 *   Copyright (C) 2008 Mathew McBride
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.bionicmessage.funambol.configuration;

import com.funambol.framework.filter.AllClause;
import com.funambol.framework.filter.Clause;
import com.funambol.framework.filter.LogicalClause;
import com.funambol.framework.filter.WhereClause;
import com.funambol.framework.server.Sync4jDevice;
import com.funambol.framework.server.inventory.DeviceInventory;
import com.funambol.framework.server.inventory.DeviceInventoryException;
import com.funambol.server.config.Configuration;

/**
 *
 * @author matt
 */
public class DeviceUtils {

    public static Sync4jDevice[] getDevicesBeginningWith(String searchName) throws DeviceInventoryException {
        Configuration c = Configuration.getConfiguration();
        DeviceInventory deviceInventory = c.getDeviceInventory();
        Clause cl = null;
        if (searchName == null || searchName.length() == 0) {
            cl = new AllClause();
        } else {
            String[] params = {searchName};
            cl = new WhereClause("id",
                    params,
                    WhereClause.OPT_START_WITH,
                    false);
        }
        return deviceInventory.queryDevices(cl);
    }

    public static Sync4jDevice getDeviceById(String devId) throws DeviceInventoryException {
        Configuration c = Configuration.getConfiguration();
        DeviceInventory deviceInventory = c.getDeviceInventory();
        Clause cl = null;
        String[] params = {devId};
        cl = new WhereClause("id",
                params,
                WhereClause.OPT_EQ,
                false);
        Sync4jDevice[] results = deviceInventory.queryDevices(cl);
        if (results.length == 1)
            return results[0];
        return null;
    }
    public static void setDevice(Sync4jDevice dev) throws DeviceInventoryException {
        Configuration c = Configuration.getConfiguration();
        DeviceInventory devInf = c.getDeviceInventory();
        devInf.setDevice(dev);
    }
    public static Sync4jDevice[] getDevicesByIds(String[] ids) throws DeviceInventoryException {
        Configuration c = Configuration.getConfiguration();
        DeviceInventory devInf = c.getDeviceInventory();
        WhereClause wcs[] = new WhereClause[ids.length];
        for (int i = 0; i < ids.length; i++) {
            String[] val = {ids[i]};
            WhereClause whereClause = new WhereClause("id",val, WhereClause.OPT_EQ,false);
            wcs[i] = whereClause;
        }
        LogicalClause lc = new LogicalClause(LogicalClause.OPT_OR,wcs);
        Sync4jDevice[] devs = devInf.queryDevices(lc);
        return devs;
    }
}
