/*
 *  UserUtils.java: Created 3rd December 2008, 9:57AM
 *   WebConfig - a web administration interface for the Funambol DS Server.
 *   Copyright (C) 2008 Mathew McBride
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.bionicmessage.funambol.configuration;

import com.funambol.framework.filter.AllClause;
import com.funambol.framework.filter.Clause;
import com.funambol.framework.filter.WhereClause;
import com.funambol.framework.server.Sync4jUser;
import com.funambol.framework.server.store.PersistentStoreException;
import com.funambol.server.admin.AdminException;
import com.funambol.server.admin.UserManager;
import com.funambol.server.config.Configuration;

/**
 *
 * @author matt
 */
public class UserUtils {

    /** Retrieves a Sync4jUser object for the specified username
     * 
     * @param name Username to retrieve information for
     * @return Sync4jUser object for user if found, or null
     * @throws com.funambol.server.admin.AdminException 
     * @throws com.funambol.framework.server.store.PersistentStoreException
     */
    public static Sync4jUser getUserByUsername(String name) throws AdminException, PersistentStoreException {
        Configuration c = Configuration.getConfiguration();
        UserManager um = c.getUserManager();
        String[] params = {name};
        WhereClause id = new WhereClause(
                "username",
                params,
                WhereClause.OPT_EQ,
                false);
        Sync4jUser[] users = um.getUsers(id);
        if (users.length == 1) {
            um.getUserRoles(users[0]);
            return users[0];
        }
        return null;
    }

    public static void setUser(Sync4jUser user) throws AdminException, PersistentStoreException {
        Configuration c = Configuration.getConfiguration();
        UserManager um = c.getUserManager();
        if (user.getRoles() == null) {
            String[] defaultRoles = {UserManager.ROLE_USER};
            user.setRoles(defaultRoles);
        }
        um.setUser(user);
    }

    public static Sync4jUser[] findUsersBeginningWith(String searchName) throws AdminException, PersistentStoreException {
        Configuration conf = Configuration.getConfiguration();
        UserManager um = conf.getUserManager();
        Clause c = null;
        if (searchName != null) {
            String[] params = {searchName};
            WhereClause wc = new WhereClause("username",
                    params,
                    WhereClause.OPT_CONTAINS,
                    false);
            c = wc;
        } else {
            c = new AllClause();
        }
        return um.getUsers(c);
    }
}
