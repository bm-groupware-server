/*
 *   WebConfig - a web administration interface for the Funambol DS Server.
 *   Copyright (C) 2008 Mathew McBride
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.bionicmessage.funambol.configuration;

import com.funambol.framework.core.*;
import com.funambol.framework.notification.NotificationException;
import com.funambol.server.config.Configuration;
import com.funambol.server.notification.NotificationEngineImpl;
import java.util.ArrayList;

/**
 * Methods for sending Funambol push notifications
 * @author matt
 */
public class NotificationUtils {

    /**
     * Copied from InboxListenerManager.java in Funambol 
     * Send notification to the device corresponding to the given
     * principal for the kinds of item in sourcesToNotify
     *
     * @param username user to notify
     * @param syncSourceEmail string
     * @param contentTypeEmail string
     */
    public static void notifyUser(String username,
            String deviceId,
            String syncSource,
            String contentType) throws NotificationException {

        ArrayList<Alert> alerts = new ArrayList<Alert>();

        Target target = new Target(syncSource);
        Meta meta = new Meta();
        meta.setType(contentType);
        Item item = new Item(null, target, meta, null, false);

        Alert alert = new Alert(new CmdID(0), false, null, 206, new Item[]{item});

        alerts.add(alert);

        Alert[] alertArray = (Alert[]) alerts.toArray(new Alert[alerts.size()]);

        Configuration conf = Configuration.getConfiguration();
        NotificationEngineImpl nei =
                new NotificationEngineImpl(conf);
        nei.sendNotificationMessage(username, deviceId, alertArray, 1);
    }
}
