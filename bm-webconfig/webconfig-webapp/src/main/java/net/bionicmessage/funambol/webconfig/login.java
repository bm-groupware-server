/*
 *   WebConfig - a web administration interface for the Funambol DS Server.
 *   Copyright (C) 2008 Mathew McBride
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.bionicmessage.funambol.webconfig;

import com.funambol.framework.core.Authentication;
import com.funambol.framework.core.Cred;
import com.funambol.framework.security.Officer;
import com.funambol.framework.server.Sync4jUser;
import com.funambol.framework.server.store.PersistentStoreException;
import com.funambol.server.admin.AdminException;
import com.funambol.server.admin.UserManager;
import com.funambol.server.config.Configuration;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author matt
 */
public class login extends HttpServlet {

    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            String username = request.getParameter("l_username");
            String password = request.getParameter("l_password");
            String utype = request.getParameter("utype");
            HttpSession session = request.getSession();
            session.removeAttribute("notloggedin");
            if (username != null && password != null) {
                try {
                    // Good, lets login
                    Configuration conf = Configuration.getConfiguration();
                    if ("admin".equals(utype)) {
                        loginAdministrator(conf, session, username, password);
                    } else {
                        loginUser(conf, session, username, password);
                    }
                    response.sendRedirect("index.jsp");
                } catch (Exception ex) {
                    ex.printStackTrace(response.getWriter());
                }
            }
        } finally {
            //out.close();
        }
    }

    protected void loginAdministrator(Configuration c,
            HttpSession session,
            String user,
            String password) throws AdminException, PersistentStoreException {
        Sync4jUser adminUser = new Sync4jUser(user, null, "", "", "", null);
        c.getUserManager().getUser(adminUser);
        if (!password.equals(adminUser.getPassword())) {
            session.setAttribute("notloggedin","Incorrect password");
            return;
        }
        try {
            if (c.getUserManager().isUniqueAdministrator(adminUser)) {
                session.setAttribute("isadministrator", "");
                session.setAttribute("user", user);
            } else {
                session.setAttribute("notloggedin", "");
            }
        } catch (Exception ex) {
            session.setAttribute("notloggedin", ex.getMessage());
            ex.printStackTrace();
        }
    }
    protected void loginUser(Configuration c,
            HttpSession session,
            String user,
            String password) {
        Officer officer = c.getOfficer();
        Authentication auth = new Authentication("syncml:auth-basic",user,password);
        Cred cred = new Cred(auth);
        Sync4jUser s4ju = officer.authenticateUser(cred);
        if (s4ju != null) {
            session.setAttribute("user",user);
        } else {
            session.setAttribute("notloggedin","");
        }
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     */
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
