HTTP Authentication officer for Funambol
----------------------------------------
The HTTP Authentication officer allows Funambol to authenticate SyncML clients
against a HTTP server, i.e a groupware server implementing GroupDAV or CalDAV.

Authentication is currently done in an OPTIONS HTTP request to the server. 
(With the username and password presented by the SyncML client encoded into
HTTP Basic details that is)
Our goal is to present some user data by HTTP headers or another DAV-friendly way 
when this is done so Funambol user and device principals can be configured 
(i.e first, last name, email, device timezone)

Installation:
Install s4j as Funambol module 
Edit config file (down in ds-server/config/net/bionicmessage/...)
Logon using the Funambol admin tool, double click on "Server settings"
and change the "Officer" value from the default to:
net/bionicmessage/funambol/httpauth/HTTPAuthenticationOfficer.xml

Comments to matt@mcbridematt.dhs.org
- Mathew McBride