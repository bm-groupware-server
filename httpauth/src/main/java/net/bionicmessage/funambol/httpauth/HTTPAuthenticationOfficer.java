/*
 * HTTPAuthenticationOfficer.java, created 7:55PM, June 3 2008.
 * (C) Mathew McBride
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation with the addition of the following permission
 * added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY FUNAMBOL, FUNAMBOL DISCLAIMS THE
 * WARRANTY OF NON INFRINGEMENT  OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses or write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA.
 *
 * You can contact Funambol, Inc. headquarters at 643 Bair Island Road, Suite
 * 305, Redwood City, CA 94063, USA, or at email address info@funambol.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License
 * version 3, these Appropriate Legal Notices must retain the display of the
 * "Powered by Funambol" logo. If the display of the logo is not reasonably
 * feasible for technical reasons, the Appropriate Legal Notices must display
 * the words "Powered by Funambol".
 */
package net.bionicmessage.funambol.httpauth;

import com.funambol.framework.core.Cred;
import com.funambol.framework.logging.FunambolLogger;
import com.funambol.framework.logging.FunambolLoggerFactory;
import com.funambol.framework.security.AbstractOfficer;
import com.funambol.framework.security.Sync4jPrincipal;
import com.funambol.framework.server.Sync4jUser;
import com.funambol.framework.server.store.NotFoundException;
import com.funambol.framework.server.store.PersistentStore;
import com.funambol.framework.server.store.PersistentStoreException;
import com.funambol.framework.tools.Base64;
import com.funambol.server.admin.UserManager;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Map;
import net.bionicmessage.groupdav.groupDAV;
import com.funambol.server.config.Configuration;

/**
 * A Funambol authentication officer which authenticates using 
 * a HTTP server and HTTP Basic authentication
 * @author matt
 */
public class HTTPAuthenticationOfficer extends AbstractOfficer {

    protected URL authPath = null;
    protected String authenticationPath = null;
    protected groupDAV gdav = null;
    protected UserManager userManager = null;
    protected PersistentStore ps = null;
    protected FunambolLogger log = null;

    public HTTPAuthenticationOfficer() {
    }
    public void init() {
        log = FunambolLoggerFactory.getLogger("funambol.auth");
        Configuration config = Configuration.getConfiguration();
        ps = config.getStore();

        userManager = (UserManager) config.getUserManager();
    }

    public Sync4jUser authenticateUser(Cred credential) {
		setClientAuth(Cred.AUTH_TYPE_BASIC);
        init();
        if (Cred.AUTH_TYPE_BASIC.equals(credential.getType())) {
            return authenticateWithBasic(credential);
        }
        // md5? umm...
        return null;
    }

    public void setAuthenticationPath(String authenticationPath) {
        this.authenticationPath = authenticationPath;
    }

    public String getAuthenticationPath() {
        return authenticationPath;
    }

    private Sync4jUser authenticateWithBasic(Cred credential) {
        try {// Lets avoid all the java net authenticator nonsense. We just want to know
            // what works
            String authenticationValue = credential.getAuthentication().getData();
            // Decode the data here, for %DOMAIN%
            byte[] auth_decoded = Base64.decode(authenticationValue.getBytes());
            String[] decoded = new String(auth_decoded).split(":");
            String user = decoded[0];
            String[] roles = {UserManager.ROLE_USER};
            try {
                String url = authenticationPath;
                if (authenticationPath.contains("%DOMAIN%")) {
                    String emailcomponents[] =
                            user.split("@");
                    String domain = emailcomponents[1];
                    url = url.replace("%DOMAIN%", domain);
                }
                url = url.replace("%USER%", user);
                authPath = new URL(url);
            } catch (MalformedURLException e) {
                throw new IllegalArgumentException(e);
            }
            gdav = new groupDAV(authenticationPath.toString(), authenticationValue);
            Map parameters = gdav.returnParamsForUser(authPath);
            if (parameters != null) {
                // Yes we're here
                // Decode the base64 junk
                String password = decoded[1];
                Sync4jUser sj = new Sync4jUser(user, password, null, null, null, roles);
                try {
                    Sync4jPrincipal principal =
                            handlePrincipal(user, credential.getAuthentication().getDeviceId());
                    ;
                } catch (PersistentStoreException e) {
                    e.printStackTrace();
                    log.error("Error handling the principal", e);
                    return null;
                }
                return sj;
            } else {
                System.out.println("No parameters returned");
                return null;
            }

        } catch (Exception e) {
            e.printStackTrace();
            log.error(this, e);
        }
        return null;
    }

    /**
     * Searchs if there is a principal with the given username and device id.
     * if no principal is found, a new one is created.
     * @param userName the user name
     * @param deviceId the device id
     * @return the found principal or the new one
     */
    protected Sync4jPrincipal handlePrincipal(String username, String deviceId)
            throws PersistentStoreException {

        Sync4jPrincipal principal = null;

        //
        // Verify if the principal for the specify deviceId and username exists
        //

        principal = getPrincipal(username, deviceId);

        if (log.isTraceEnabled()) {
            log.trace("Principal '" + username +
                    "/" +
                    deviceId + "' " +
                    ((principal != null) ? "found" : "not found. A new principal will be created"));
        }

        if (principal == null) {
            principal = insertPrincipal(username, deviceId);
            if (log.isTraceEnabled()) {
                log.trace("Principal '" + username +
                        "/" +
                        deviceId + "' created");
            }
        }

        return principal;
    }

    protected Sync4jPrincipal getPrincipal(String userName, String deviceId)
            throws PersistentStoreException {

        Sync4jPrincipal principal = null;

        //
        // Verify that exist the principal for the specify deviceId and username
        //
        principal = Sync4jPrincipal.createPrincipal(userName, deviceId);

        try {
            ps.read(principal);
        } catch (NotFoundException ex) {
            return null;
        }

        return principal;
    }

    /**
     * Inserts a new principal with the given userName and deviceId
     * @param userName the username
     * @param deviceId the device id
     * @return the principal created
     * @throws PersistentStoreException if an error occurs creating the principal
     */
    protected Sync4jPrincipal insertPrincipal(String userName, String deviceId)
            throws PersistentStoreException {

        //
        // We must create a new principal
        //
        Sync4jPrincipal principal =
                Sync4jPrincipal.createPrincipal(userName, deviceId);

        ps.store(principal);

        return principal;
    }
}
