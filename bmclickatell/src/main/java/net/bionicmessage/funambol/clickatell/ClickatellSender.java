/*
 * Funambol is a mobile platform developed by Funambol, Inc.
 * Copyright (C) 2007 Funambol, Inc.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation with the addition of the following permission
 * added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY FUNAMBOL, FUNAMBOL DISCLAIMS THE
 * WARRANTY OF NON INFRINGEMENT  OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses or write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA.
 *
 * You can contact Funambol, Inc. headquarters at 643 Bair Island Road, Suite
 * 305, Redwood City, CA 94063, USA, or at email address info@funambol.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License
 * version 3, these Appropriate Legal Notices must retain the display of the
 * "Powered by Funambol" logo. If the display of the logo is not reasonably
 * feasible for technical reasons, the Appropriate Legal Notices must display
 * the words "Powered by Funambol".
 */
package net.bionicmessage.funambol.clickatell;

import com.funambol.framework.notification.Message;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.funambol.framework.logging.Sync4jLogger;
import com.funambol.framework.notification.NotificationException;
import com.funambol.framework.notification.sender.Sender;
import com.funambol.framework.server.*;
// import com.funambol.framework.tools.DbgTools;
import com.funambol.framework.tools.DbgTools;
import com.funambol.server.notification.sender.WDPHeader;
import com.funambol.server.notification.sender.WSPHeader;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

/**
 * This class is an implementation of Sender interface. It sends the
 * notification message over WAP
 *
 * 
 */
public class ClickatellSender implements Sender {
    // ------------------------------------------------------------ Private data
    private Logger log = null;
    private String clickatellUser = null;
    private String clickatellPass = null;
    private String clickatellID = null;
    private String clickatellURL = "http://api.clickatell.com/http/sendmsg?api_id=%s&user=%s&password=%s&to=%s&udh=%s&data=%s";
    private WDPHeader wdpHeader = null;
    private WSPHeader wspHeader = null;
    // ------------------------------------------------------------- Constructor
    public ClickatellSender() {
        log = Sync4jLogger.getLogger();
    }
    // ---------------------------------------------------------- Public Methods
    /**
     * Prepares the notification message (adds the wap header) and calls
     * abstract method sendMessage() to send the message
     *
     * @param deviceAddress address of the device where message should be sent
     * @param message       array of bytes containing the notification message
     *                      that has to be sent
     */
    public void sendNotificationMessage(Sync4jDevice device, Message msg)
            throws NotificationException {

        byte[] message = msg.getMessageContent();
        String deviceAddress = device.getMsisdn();

        if (device.getDeviceId().startsWith("fwm")) {
            wdpHeader = new WDPHeader();
        } else {
            wdpHeader = new WDPHeader(50001);
        }
        if (deviceAddress.length() == 0) {
            return; // No MSISDN
        }

        if (log.isLoggable(Level.INFO)) {
            log.info("sending message: " + DbgTools.bytesToHex(message));
            log.info("to: " + deviceAddress);
        }
        // Uncomment if yuo want to save the message to send
        //writeByte(message, phoneNumber);

        wspHeader = new WSPHeader(WSPHeader.CONTENT_TYPE_CODE_NOTIFICATION);
        String udhPortion = getUserDataHeader();
        String wspPortion = getWspHeader();
        String totalData = wspPortion + DbgTools.bytesToHex(message);

        String clickatellUrl = clickatellURL.replace("<%APPID%>", this.getClickatellID());
        clickatellUrl = clickatellUrl.replace("<%USER%>",this.getClickatellUser());
        clickatellUrl = clickatellUrl.replace("<%PASS%>",this.getClickatellPass());
        clickatellUrl = clickatellUrl.replace("<%UDH%>", udhPortion);
        clickatellUrl = clickatellUrl.replace("<%DATA%>",totalData);
        clickatellUrl = clickatellUrl.replace("<%TO%>",device.getMsisdn());
        try {
            URL ur = new URL(clickatellUrl);
            InputStream rsStream = ur.openStream();
            BufferedReader rReader = new BufferedReader(new InputStreamReader(rsStream));
            String clickatellId = rReader.readLine();
            log.info("Clickatell returned: " + clickatellId);
            rReader.close();
        } catch (Exception e) {
            throw new NotificationException("Error sending notification: ", e);
        }
    }

    private String getUserDataHeader() {
        byte[] wdpHeaderContent = wdpHeader.getHeader();
        return DbgTools.bytesToHex(wdpHeaderContent);
    }

    private String getWspHeader() {
        byte[] wspHeaderContent = wspHeader.getHeader();
        return DbgTools.bytesToHex(wspHeaderContent);
    }
    // BEAN methods
    public String getClickatellUser() {
        return this.clickatellUser;
    }

    public void setClickatellUser(String user) {
        this.clickatellUser = user;
    }

    public String getClickatellPass() {
        return this.clickatellPass;
    }

    public void setClickatellPass(String ps) {
        this.clickatellPass = ps;
    }

    public String getClickatellID() {
        return this.clickatellID;
    }

    public void setClickatellID(String id) {
        this.clickatellID = id;
    }

    public void setClickatellURL(String clickatellURL) {
        this.clickatellURL = clickatellURL;
    }

    public String getClickatellURL() {
        return clickatellURL;
    }
    
}
